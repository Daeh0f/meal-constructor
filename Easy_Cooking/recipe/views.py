# -*- coding: utf-8 -*-
import json
from django.shortcuts import render, render_to_response
from .models import Ingredients, Goods, Meal, Price, Market

def index(request):
    '''q = request.GET.get("q", "")
    entries = get_goods_by_type(q)

    return render(request, 'index.html', {
        'entries': entries,
    })'''
    pass

def main(request):
    return render_to_response('main.html', {})

def sorted_list(request, meal_type):
    print(meal_type)
    if not meal_type:
        meals_qs = Meal.objects.all()
    else:
        meals_qs = Meal.objects.filter(meal_type=meal_type)
    return render_to_response('sorted_list.html', {"meals": meals_qs})

def detail(request, meal_name):
    meal = get_meal_by_name(meal_name)
    ingredients = get_ingredients_by(meal_name)
    goods = []
    for ing in ingredients:
        for g in get_goods_by(ing.ingredients_type):
            h = {}
            h["name"] = g.goods_title
            h["prices"] = get_goods_info_by(g.goods_title)
            goods.append(h)

    return render_to_response('recept.html',
                              {"meal": meal, "ingredients": ingredients, "goods": goods})

def get_meal_by_name(meal_name):
    meals_qs = Meal.objects.get(meal_title=meal_name)
    return meals_qs

def get_ingredients_by(meal_name):
    meal_qs = Meal.objects.get(meal_title=meal_name)
    return Ingredients.objects.filter(ingredients_meal_id=meal_qs.id)

def get_goods_by(goods_type_):
    print("gt: ", goods_type_)
    goods_qs = Goods.objects.filter(goods_type=goods_type_)
    print("goods: ", goods_qs)
    return goods_qs

def get_meal_by(meal_type):
    meals_qs = Meal.objects.get(meal_type=meal_type)
    return meals_qs

'''
Возвращает словарь с информацией о товаре заданного наименования
Ключи словаря:
market - имя магаза
price  - цена данного товара в магазине
price_measure - за что указана цена(граммы/штуки)
'''
def get_goods_info_by(goods_title_):
    goods_info = []

    goods_id = Goods.objects.get(goods_title=goods_title_).id
    prices_qs = Price.objects.filter(price_goods_id=goods_id)

    for price_entry in prices_qs:
        current_market = Market.objects.get(market_title=price_entry.price_market_id).market_title

        goods_info.append({
            'price': price_entry.price_value,
            'price_measure': price_entry.price_value_measure,
            'market': current_market
        })

    return goods_info
