from django.contrib import admin

from models import Goods, Market, Meal, Ingredients, Price



class IngredientsAdmin (admin.ModelAdmin):
    fields = ['ingredients_meal_id', 'ingredients_type', 'ingredients_amount', 'ingredients_amount_measure']


admin.site.register(Ingredients, IngredientsAdmin)
admin.site.register(Goods)
admin.site.register(Market)
admin.site.register(Meal)
admin.site.register(Price)

