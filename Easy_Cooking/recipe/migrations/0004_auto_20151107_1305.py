# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0003_auto_20151107_0230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='price_market_id',
            field=models.ForeignKey(to='recipe.Market'),
        ),
    ]
