# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Goods',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('goods_title', models.CharField(max_length=100)),
                ('goods_type', models.CharField(max_length=100)),
                ('goods_amount', models.DecimalField(max_digits=12, decimal_places=5)),
                ('goods_amount_measure', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Market',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('market_phone', models.BigIntegerField()),
                ('market_address', models.CharField(max_length=200)),
                ('market_working_time', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Meal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meal_title', models.CharField(max_length=100)),
                ('meal_description', models.CharField(max_length=500)),
                ('meal_guide', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price_value', models.DecimalField(max_digits=12, decimal_places=5)),
                ('price_goods_id', models.ForeignKey(to='recipe.Goods')),
                ('price_market_id', models.ForeignKey(to='recipe.Market')),
            ],
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipe_amount', models.DecimalField(max_digits=12, decimal_places=5)),
                ('recipe_amount_measure', models.CharField(default=b'\xd0\xb3\xd1\x80', max_length=10)),
                ('recipe_goods_id', models.ForeignKey(to='recipe.Goods')),
                ('recipe_meal_id', models.ForeignKey(to='recipe.Meal')),
            ],
        ),
    ]
