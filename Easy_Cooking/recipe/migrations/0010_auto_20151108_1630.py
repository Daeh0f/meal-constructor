# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0009_auto_20151108_1547'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='price_value',
            field=models.DecimalField(max_digits=12, decimal_places=1),
        ),
    ]
