# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0004_auto_20151107_1305'),
    ]

    operations = [
        migrations.AddField(
            model_name='price',
            name='price_value_measure',
            field=models.CharField(default=b'\xd0\xba\xd0\xb3', max_length=10),
        ),
    ]
