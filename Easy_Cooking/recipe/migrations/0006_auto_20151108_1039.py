# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0005_price_price_value_measure'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal',
            name='meal_type',
            field=models.CharField(default=b'\xd0\xa1\xd1\x83\xd0\xbf', max_length=100),
        ),
        migrations.AlterField(
            model_name='price',
            name='price_value_measure',
            field=models.CharField(default=b'\xd0\xb3\xd1\x80', max_length=10),
        ),
    ]
