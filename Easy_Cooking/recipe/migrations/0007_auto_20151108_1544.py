# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0006_auto_20151108_1039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goods',
            name='goods_amount',
            field=models.DecimalField(max_digits=12, decimal_places=1),
        ),
    ]
