# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0002_market_market_title'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ingredients_amount', models.DecimalField(max_digits=12, decimal_places=5)),
                ('ingredients_amount_measure', models.CharField(default=b'\xd0\xb3\xd1\x80', max_length=10)),
                ('ingredients_meal_id', models.ForeignKey(to='recipe.Meal')),
                ('ingredients_type', models.ForeignKey(to='recipe.Goods')),
            ],
        ),
        migrations.RemoveField(
            model_name='recipe',
            name='recipe_goods_id',
        ),
        migrations.RemoveField(
            model_name='recipe',
            name='recipe_meal_id',
        ),
        migrations.AlterField(
            model_name='price',
            name='price_market_id',
            field=models.OneToOneField(to='recipe.Market'),
        ),
        migrations.DeleteModel(
            name='Recipe',
        ),
    ]
