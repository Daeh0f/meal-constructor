# -*- coding: utf-8 -*-
from django.db import models

class Goods(models.Model):
    goods_title = models.CharField(max_length=100) #название
    goods_type = models.CharField(max_length=100) #вид - кетчуп, макароты
    goods_amount = models.DecimalField(max_digits=12, decimal_places=1) #количество
    goods_amount_measure = models.CharField(max_length=10) # единицы измерения

    def __unicode__(self):
        return u"{}".format(self.goods_type)

class Market(models.Model):
    market_title = models.CharField(max_length=100, default="market")
    market_phone = models.BigIntegerField()
    market_address = models.CharField(max_length=200)
    market_working_time = models.CharField(max_length=100)

    def __unicode__(self):
        return u"{}".format(self.market_title)

class Meal(models.Model):
    meal_title = models.CharField(max_length=100)
    meal_description = models.CharField(max_length=500)
    meal_guide = models.TextField()
    meal_type = models.CharField(max_length=100, default="Суп")

    def __unicode__(self):
        return u"{}".format(self.meal_title)

class Ingredients(models.Model):
    ingredients_meal_id = models.ForeignKey(Meal)
    ingredients_type = models.ForeignKey(Goods)
    ingredients_amount = models.DecimalField(max_digits=12, decimal_places=1)
    ingredients_amount_measure = models.CharField(max_length=10, default="гр")

    def __unicode__(self):
        return u"{}: {}".format(self.ingredients_meal_id, self.ingredients_type)

class Price(models.Model):
    price_goods_id = models.ForeignKey(Goods)
    price_market_id = models.ForeignKey(Market)
    price_value = models.DecimalField(max_digits=12, decimal_places=1)
    price_value_measure = models.CharField(max_length=10, default="гр")

    def __unicode__(self):
        return u"{}: {}".format(self.price_market_id, self.price_value)
