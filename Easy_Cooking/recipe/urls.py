#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from recipe.views import index, main, sorted_list, detail

urlpatterns = [

 #url(r'^index', index, name='index'),
 #url(r'^(?P<ingredients_id>\d+)', recept, name='recept'),
 # ex: /recipes/Овощи/
 url(r'^detail/(?P<meal_name>[ ,\w]+)$', detail),
 url(r'^type/(?P<meal_type>[ \w]*)$', sorted_list),
 #url(r'^calculate/([])$', sorted_list),
]